**shiro-redis**

shiro集成redis的适配器，为解决shiro-ehcache不利于集群而打造的缓存集群方案。

**Maven坐标**
```
<dependency>
    <groupId>org.iherus.shiro</groupId>
    <artifactId>shiro-redis</artifactId>
    <version>1.1.0</version>
</dependency>
```
**shiro-redis使用说明**

1、基于ini的使用方式
```
    [main]
    #定义凭证匹配器
    credentialsMatcher=org.apache.shiro.authc.credential.HashedCredentialsMatcher
    #散列算法
    credentialsMatcher.hashAlgorithmName=MD5
    #散列次数
    credentialsMatcher.hashIterations=2

    #定义缓存池配置
    poolConfig=redis.clients.jedis.JedisPoolConfig
    poolConfig.minIdle=3
    poolConfig.maxIdle=20
    poolConfig.maxWaitMillis=1000
    poolConfig.maxTotal=300
    #定义缓存配置工厂
    configFactory=org.iherus.shiro.cache.redis.RedisCacheConfigFactory
    configFactory.poolConfig=$poolConfig
    #定义缓存管理器
    cacheManager=org.iherus.shiro.cache.redis.RedisCacheManager
    cacheManager.configFactory=$configFactory

    #将凭证匹配器设置到realm
    customRealm=org.iherus.shiro.tester.CustomRealm
    customRealm.credentialsMatcher=$credentialsMatcher
    securityManager.realms=$customRealm
    securityManager.cacheManager=$cacheManager
```
详细测试代码请看：src/test/java/org/iherus/shiro /tester/SimpleCacheTest.java

2、Shiro+Spring集成的方式
```
    <!-- SHIRO安全管理器 -->
    <bean id="securityManager" class="org.apache.shiro.web.mgt.DefaultWebSecurityManager">
        <property name="realm" ref="userRealm" />
        <property name="cacheManager" ref="cacheManager" />
        <property name="rememberMeManager" ref="rememberMeManager" />
        <property name="sessionManager" ref="sessionManager" />
    </bean>
    <!-- 用户域 -->
    <bean id="userRealm" class="com.iherus.arch4j.modules.authz.realm.SimpleUserRealm" />
    <!-- 会话管理器-->
    <bean id="sessionManager" class="org.apache.shiro.web.session.mgt.DefaultWebSessionManager">
        <property name="sessionDAO" ref="sessionDAO" />
        <property name="globalSessionTimeout" value="${shiro.globalSessionTimeout}" />
        <property name="deleteInvalidSessions" value="${shiro.deleteInvalidSessions}" />
        <property name="sessionValidationInterval" value="${shiro.sessionValidationInterval}" />
    </bean>
    <!-- sessionDAO-->
    <bean id="sessionDAO" class="org.apache.shiro.session.mgt.eis.EnterpriseCacheSessionDAO">
	<property name="cacheManager" ref="cacheManager" />
    </bean>
    <!-- 记住登录管理器 -->
    <bean id="rememberMeManager" class="org.apache.shiro.web.mgt.CookieRememberMeManager">
        <property name="cookie" ref="rememberMeCookie" />
    </bean>
    <!-- Cookie对象 -->
    <bean id="rememberMeCookie" class="org.apache.shiro.web.servlet.SimpleCookie">
        <property name="name" value="${shiro.cookieName}" />
        <property name="maxAge" value="${shiro.cookieMaxAge}" />
    </bean>
    <!-- 缓存管理器 -->
    <bean id="cacheManager" class="org.iherus.shiro.cache.redis.RedisCacheManager">
        <property name="configFactory" ref="configFactory" />
    </bean>
    <!-- 缓存配置工厂 -->
    <bean id="configFactory" class="org.iherus.shiro.cache.redis.RedisCacheConfigFactory">
        <property name="host" value="${redis.host}" />
        <property name="port" value="${redis.port}" />
        <property name="timeout" value="${redis.timeout}" />
        <property name="database" value="${shiro.cacheDatabase}" />
        <property name="poolConfig" ref="poolConfig" />
    </bean>
    <!-- 配置缓存连接池 -->
    <bean id="poolConfig" class="redis.clients.jedis.JedisPoolConfig">
        <property name="minIdle" value="${redis.minIdle}" />
        <property name="maxIdle" value="${redis.maxIdle}" />
        <property name="maxWaitMillis" value="${redis.maxWaitMillis}" />
        <property name="maxTotal" value="${redis.maxTotal}" />
    </bean>
```

更多正在补充中。。。。。 :smile:


**Features**

欢迎提出更好的意见，帮助完善 shiro-redis

**Copyright**

Apache License, Version 2.0